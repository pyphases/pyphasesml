import unittest

import numpy as np
from pyPhases import classLogger
from pyPhasesRecordloader import RecordSignal, Signal

from pyPhasesML.FeatureExtraction import FeatureExtraction


@classLogger
class TestFeatureExtraction(unittest.TestCase):
    def setUp(self):
        class MyFeatureExtraction(FeatureExtraction):
            def dummy_step(self, recordSignal: RecordSignal, **options) -> Signal:
                return recordSignal.getSignalByName("test")

            def multiple(self, recordSignal: RecordSignal, **options) -> Signal:
                return [recordSignal.getSignalByName("test"), recordSignal.getSignalByName("test")]

        self.fe = MyFeatureExtraction()

    def test_addSingle(self):
        # Test adding a numpy array
        record_signal = RecordSignal()
        name = "test_array"
        array = np.array([1, 2, 3])
        self.fe.addSingle(name, record_signal, array)

        self.assertTrue(name in record_signal.signalNames)
        self.assertIsInstance(record_signal.getSignalByName(name), Signal)

        # Test adding a Signal object
        record_signal = RecordSignal()
        name = "test_signal"
        signal = Signal(name, array, frequency=100)

        self.fe.addSingle(name, record_signal, signal)
        self.assertTrue(name in record_signal.signalNames)
        self.assertIsInstance(record_signal.getSignalByName(name), Signal)

    def test_step(self):
        # Test calling a step method that exists
        record_signal = RecordSignal()
        signal = Signal("test", np.array([1, 2, 3]), frequency=100)
        record_signal.addSignal(signal, "test")
        result = self.fe.step("dummy_step", record_signal)
        self.assertEqual(result, signal)

        # Test calling a step method that doesn't exist
        with self.assertRaises(Exception):
            self.fe.step("nonexistent_step", record_signal)

    def test_extractChannelsByConfig(self):
        # Test extracting a single channel
        record_signal = RecordSignal()
        signal = Signal("test", np.array([1, 2, 3]), frequency=100)
        record_signal.addSignal(signal, "test")
        config = [{"name": "dummy_step", "channels": ["test"]}]
        self.fe.extractChannelsByConfig(record_signal, config)
        self.assertTrue("test" in record_signal.signalNames)

        # Test extracting multiple channels
        record_signal = RecordSignal()
        signal1 = Signal("test", np.array([1, 2, 3]), frequency=100)
        signal2 = Signal("test2", np.array([4, 5, 6]), frequency=100)
        record_signal.addSignal(signal1, "test")
        record_signal.addSignal(signal2, "test2")
        config = [{"name": "multiple", "channels": ["test1", "test2"]}]
        self.fe.extractChannelsByConfig(record_signal, config)
        self.assertTrue("test" in record_signal.signalNames)
        self.assertTrue("test2" in record_signal.signalNames)

    def test_call(self):
        # Test extracting a single channel
        record_signal = RecordSignal()
        signal = Signal("test", np.array([1, 2, 3]), frequency=100)
        record_signal.addSignal(signal, "test")
        config = [{"name": "dummy_step", "channels": ["test"]}]
        self.fe(record_signal, config)
        self.assertTrue("test" in record_signal.signalNames)