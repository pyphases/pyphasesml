import sys
import unittest
from unittest.mock import MagicMock, Mock, patch

import numpy as np

from pyPhasesML import TrainingSetLoader

sys.modules["torch"] = MagicMock()
sys.modules["torch.optim"] = MagicMock()

from pyPhasesML.adapter.ModelTorchAdapter import ModelTorchAdapter


class MockType(MagicMock):
    def __instancecheck__(self, instance):
        return False


class TestModelTorchAdapter(unittest.TestCase):
    def setUp(self):
        # Mocking torch dependencies
        sys.modules["torch"].cuda.is_available.return_value = False  # Mocking GPU availability
        sys.modules["torch"].nn.Module = MagicMock

        # Mock other dependencies
        self.mock_data_set = MagicMock(spec=TrainingSetLoader)
        self.mock_trainingData = [MagicMock()], [MagicMock()]
        self.mock_validationData = [MagicMock()], [MagicMock()]
        self.mock_training_set_loader = TrainingSetLoader([self.mock_trainingData], [self.mock_validationData])
        # self.mock_scorer_torch = MagicMock()

        # Instantiate the object under test
        self.adapter = ModelTorchAdapter()
        self.adapter.config.numClasses = 2

    def test_init(self):
        # self.assertFalse(self.adapter.useGPU)  # Assuming GPU is not available in this test
        self.assertFalse(self.adapter.skipValidation)
        self.assertEqual(self.adapter.metricDefinitions, {})

    @patch("pyPhasesML.adapter.ModelTorchAdapter.SystemCheckPoint")
    @patch("pyPhasesML.adapter.ModelTorchAdapter.CheckPoint")
    @patch("pyPhasesML.adapter.ModelTorchAdapter.CSVLogger")
    @patch("pyPhasesML.adapter.ModelTorchAdapter.LoadOptimizer")
    def test_initAdapter(self, mock_LoadOptimizer, mock_CSVLogger, mock_CheckPoint, mock_SystemCheckPoint):
        # Mock configurations
        mock_config = MagicMock()
        self.adapter.config = mock_config

        mock_LoadOptimizer().priority = 10
        mock_CSVLogger().priority = 10
        mock_CheckPoint().priority = 10
        mock_SystemCheckPoint().priority = 1

        # Call the method
        self.adapter.initAdapter()

        # Check if callbacks are registered
        self.assertEqual(len(self.adapter.cbs), 4)  # Assuming 4 callbacks are registered
        self.assertEqual(mock_SystemCheckPoint.return_value, self.adapter.cbs[0])
        self.assertEqual(mock_LoadOptimizer.return_value, self.adapter.cbs[1])
        self.assertEqual(mock_CSVLogger.return_value, self.adapter.cbs[2])
        self.assertEqual(mock_CheckPoint.return_value, self.adapter.cbs[3])

    @patch("pyPhasesML.adapter.ModelTorchAdapter.np.isnan", return_value=False)
    def test_train(self, mock_isnan):
        # Mock additional dependencies
        loss = MagicMock()
        lossFunction = MagicMock(return_value=loss)

        self.adapter.getLossFunction = MagicMock(return_value=lossFunction)
        self.adapter.optimizer = MagicMock()
        self.adapter.model = MagicMock(side_effect=lambda x: x)
        self.adapter.maxEpochs = 1
        batchFeats, targs = MagicMock(), MagicMock()
        targs.__len__.return_value = 1
        targs.isnan.return_value = np.array([False])
        batchFeats.isnan.return_value = np.array([False])
        
        self.adapter.prepareDataAdapter = MagicMock(return_value=(batchFeats, targs))
        self.adapter.mapOutputForLoss = MagicMock(side_effect=lambda x: x)
        self.adapter.validate = MagicMock()

        # training
        self.adapter.train(self.mock_training_set_loader)

        self.adapter.getLossFunction.assert_called_with()
        self.adapter.prepareDataAdapter.assert_called_with(self.mock_trainingData)

        self.adapter.optimizer.zero_grad.assert_called()
        self.adapter.model.assert_called_with(batchFeats)
        self.adapter.mapOutputForLoss.assert_called_with(batchFeats)
        lossFunction.assert_called_with(batchFeats, targs)
        loss.backward.assert_called()
        self.adapter.optimizer.step.assert_called()

        self.assertEqual(self.adapter.fullEpochs, 1)

    @patch('torch.no_grad')
    @patch('torch.is_tensor', return_value=True)
    @patch('torch.argmax')
    def test_predict(self, mock_argmax, mock_is_tensor, mock_no_grad):
        # Arrange
        mock_model = Mock()
        mock_model.return_value = 5
        adapter = ModelTorchAdapter()
        adapter.getModelEval = Mock(return_value=mock_model)
        adapter.prepareX = Mock(return_value='input')
        adapter.mapOutputForPrediction = Mock(return_value=10)
        adapter.useGPU = False
        adapter.config.numClasses = 0
        adapter.oneHotDecoded = False
        input_data = np.arange(10).reshape(1, 5, 2)
        
        # Act
        result = adapter.predict(input_data, returnNumpy=False)
        
        # Assert
        mock_model.assert_called_once_with('input')
        mock_argmax.assert_not_called()
        self.assertEqual(result, 10)