import unittest
from unittest.mock import MagicMock

from pyPhasesML.Model import ModelConfig
from pyPhasesML.adapter.torch.Callback import Callback


class TestCallback(unittest.TestCase):
    def setUp(self):
        self.config = ModelConfig()
        self.callback = Callback(self.config)
        self.model = MagicMock()
        self.dataset = MagicMock()

    def test_trigger_training_start(self):
        self.callback.onTrainingStart = MagicMock()
        self.callback.trigger("trainingStart", model=self.model, dataset=self.dataset)
        self.callback.onTrainingStart.assert_called_once_with(model=self.model, dataset=self.dataset)

    def test_on_training_end(self):
        self.callback.onTrainingEnd = MagicMock()
        self.callback.trigger("trainingEnd")
        self.callback.onTrainingEnd.assert_called_once()

    def test_on_validation_start(self):
        self.callback.onValidationStart = MagicMock()
        self.callback.trigger("validationStart")
        self.callback.onValidationStart.assert_called_once()

    def test_on_validation_end(self):
        self.callback.onValidationEnd = MagicMock()
        self.callback.trigger("validationEnd")
        self.callback.onValidationEnd.assert_called_once()

    def test_on_batch_end(self):
        self.callback.onBatchEnd = MagicMock()
        self.callback.trigger("batchEnd")
        self.callback.onBatchEnd.assert_called_once()
