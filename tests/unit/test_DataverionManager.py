import unittest

from pyPhases import classLogger
from pyPhases.test import mockLogger
from pyPhasesML.DataversionManager import DataversionManager

@classLogger
class TestDataversionManager(unittest.TestCase):
    def setUp(self):
        self.groupedRecords = {
            "group1": ["record1", "record2", "record3"],
            "group2": ["record4", "record5", "record6"],
            "group3": ["record7", "record8", "record9"],
            "group4": ["record10", "record11", "record12"],
            "group5": ["record13", "record14", "record15"],
        }
        self.splits = {"train": ["0:1"], "val": ["1:3"], "test": ["3:5"]}
        self.seed = 123
        self.dataversion_manager = DataversionManager(self.groupedRecords, self.splits, self.seed)

    def test_shuffle(self):
        expect_groups = ["group2", "group4", "group5", "group1", "group3"]
        self.assertListEqual(list(self.dataversion_manager.groupedRecords.keys()), expect_groups)

    def test_groupDatasetBySplit(self):
        # Test if the method returns the expected output for a given input
        datasetName = "train"
        expected_output = ["record1", "record2", "record3"]
        output = self.dataversion_manager.groupDatasetBySplit(datasetName, self.splits, self.groupedRecords)
        self.assertListEqual(output, expected_output)

    def test_groupDatasetsBySplit(self):
        # Test if the method returns the expected output for a given input
        datasetNames = ["train", "val", "test"]
        expected_output = {
            "train": ["record1", "record2", "record3"],
            "val": ["record4", "record5", "record6", "record7", "record8", "record9"],
            "test": ["record10", "record11", "record12", "record13", "record14", "record15"],
        }
        output = self.dataversion_manager.groupDatasetsBySplit(datasetNames, self.splits, self.groupedRecords)
        self.assertDictEqual(output, expected_output)

    def test_getRecordsForSplit(self):
        # records are shuffled
        datasetName = "train"
        expected_output = ["record4", "record5", "record6"]
        output = self.dataversion_manager.getRecordsForSplit(datasetName)
        self.assertListEqual(output, expected_output)

    def test_validatDatasetVersion_unique(self):
        # raises exception on duplicate records
        recordGroups = {
            "train": ["record1", "record2", "record3"],
            "val": ["record4"],
            "test": ["record10", "record1"],
        }
        self.dataversion_manager.groupedRecords = recordGroups
        with self.assertRaises(Exception):
            self.dataversion_manager.validatDatasetVersion(raiseException=True)

    def test_validatDatasetVersion_complete(self):
        splits = {"train": ["0:1"], "val": ["1:3"], "test": ["3:4"]}
        self.dataversion_manager.splits = splits

        self.dataversion_manager = DataversionManager(self.groupedRecords, splits)
        with self.assertRaises(Exception):
            self.dataversion_manager.validatDatasetVersion(raiseException=True)

    def test_removeRecordIndexesFromSplit(self):
        # Test if the method removes the specified records from the specified dataset
        datasetName = "train"
        recordIndexes = [1, 2]
        self.dataversion_manager.removeRecordIndexesFromSplit(datasetName, recordIndexes)
        removedRecords = self.dataversion_manager.removedRecords.get(datasetName, None)
        expected_removedRecords = [1, 2]
        self.assertEqual(removedRecords, expected_removedRecords)

    def test_groupDatasetBySplit_removed(self):
        # Test if the method returns the expected output for a given input
        datasetName = "train"
        expected_output = ["record1"]
        output = self.dataversion_manager.groupDatasetBySplit(datasetName, self.splits, self.groupedRecords, [1, 2])
        self.assertListEqual(output, expected_output)

    @mockLogger
    def test_complete(self, mockLog):
        self.dataversion_manager.validatDatasetVersion()
        mockLog.assertSuccessLike("All records are unique and present in the dataset splits")

    @mockLogger
    def test_seed(self, mockLog):
        # Test if the method returns the expected output for a given input

        expected_output = {
            "train": ["record4", "record5", "record6"],
            "val": ["record10", "record11", "record12", "record13", "record14", "record15"],
            "test": ["record1", "record2", "record3", "record7", "record8", "record9"],
        }
        self.assertEqual(expected_output["val"], self.dataversion_manager.getRecordsForSplit("val"))
        self.assertEqual(expected_output["train"], self.dataversion_manager.getRecordsForSplit("train"))
        self.assertEqual(expected_output["test"], self.dataversion_manager.getRecordsForSplit("test"))
