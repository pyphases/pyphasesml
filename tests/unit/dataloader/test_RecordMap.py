import unittest

from pyPhasesML.datapipes.RecordMap import RecordMap


class TestRecordMap(unittest.TestCase):
    def setUp(self):
        self.dataset = ["a", "b", "c", "d", "e"]
        self.mapping = [0, 2, 4]
        self.record_map = RecordMap(self.dataset, self.mapping)

    def test_len(self):
        self.assertEqual(len(self.record_map), 3)

    def test_getitem(self):
        self.assertEqual(self.record_map[0], "a")
        self.assertEqual(self.record_map[1], "c")
        self.assertEqual(self.record_map[2], "e")

    def test_iter(self):
        expected_output = ["a", "c", "e"]
        for i, item in enumerate(self.record_map):
            self.assertEqual(item, expected_output[i])


    def test_segments(self):
        # segment-indexes: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
        # record-indexes:  0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3 , 4
        
        dataset = ["a1", "a2", "b1", "b2", "b3", "b4", "c1", "c2", "c3", "d1", "d2", "e1"]
        mapping = [1, 3, 2]
        segment_lengths = [2, 4, 3, 2, 1]
        expectedMapping =  [2, 3, 4, 5, 9, 10, 6, 7, 8]


        rm = RecordMap(dataset, mapping, segment_lengths)

        self.assertEqual(expectedMapping, rm.mapping)
        self.assertEqual(["b1", "b2", "b3", "b4", "d1", "d2", "c1", "c2", "c3"], [rm[i] for i in range(len(rm))])
