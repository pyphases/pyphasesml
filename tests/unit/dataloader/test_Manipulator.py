import unittest
from unittest.mock import MagicMock

from pyPhasesML.datapipes.Manipulator import Manipulator
from pyPhasesML.DataManipulation import DataManipulation


class TestManipulator(unittest.TestCase):

    def test_init(self):
        datapipe = MagicMock()
        manipulation = MagicMock(spec=DataManipulation)
        config = MagicMock()
        
        manipulator = Manipulator(datapipe, manipulation, config)

        self.assertEqual(manipulator.datapipe, datapipe)
        self.assertEqual(manipulator.manipulation, manipulation)
        self.assertEqual(manipulator.config, config)

    def test_getitem(self):
        datapipe = [1, 2, 3]
        manipulation = MagicMock(return_value='manipulated')
        config = MagicMock()
        
        manipulator = Manipulator(datapipe, manipulation, config)

        self.assertEqual(manipulator[0], 'manipulated')
        manipulation.assert_called_once_with(1, config, 0)

    def test_len(self):
        datapipe = MagicMock(__len__=MagicMock(return_value=5))
        manipulation = MagicMock()
        config = MagicMock()
        
        manipulator = Manipulator(datapipe, manipulation, config)

        self.assertEqual(len(manipulator), 5)
        datapipe.__len__.assert_called_once()

    def test_iteration(self):
        datapipe = [1, 2, 3]
        manipulation = MagicMock(return_value='manipulated')
        config = MagicMock()
        
        manipulator = Manipulator(datapipe, manipulation, config)

        for i, item in enumerate(manipulator):
            self.assertEqual(item, 'manipulated')
            manipulation.assert_called_with(datapipe[i], config, i)