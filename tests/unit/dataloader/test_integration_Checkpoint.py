import sys
import unittest
from pathlib import Path

from unittest.mock import MagicMock, call, patch

sys.modules["torch"] = MagicMock()
sys.modules["torch.optim"] = MagicMock()


from pyPhasesML.adapter.torch.CheckPoint import CheckPoint
from pyPhasesML.adapter.torch.SystemCheckPoint import SystemCheckPoint
from pyPhasesML.adapter.torch.FindLearningRate import FindLearningRate


class TestSystemCheckPoint(unittest.TestCase):
    
    def setUp(self):
        self.torchMock = sys.modules["torch"]
        self.torchMock.cuda.max_memory_allocated.return_value = 12345.0
        self.torchMock.nn.Module = type('Module', (object,), {})
    
    def tearDown(self):
        patch.stopall()

    @patch("pickle.dump")
    @patch("builtins.open")
    def test_createCheckpoint(self, open, dump):
        from pyPhasesML.adapter.ModelTorchAdapter import ModelTorchAdapter
        modelTorchAdapter = ModelTorchAdapter()
        modelTorchAdapter.model = MagicMock()
        modelTorchAdapter.bestModelPath = None
        modelTorchAdapter.config = MagicMock()
        modelTorchAdapter.config.logPath = "."
        modelTorchAdapter.config.stopAfterNotImproving = 0
        modelTorchAdapter.optimizer = MagicMock()
        scorerResults = MagicMock()
        scorer = MagicMock()
        dataset = MagicMock()

        modelTorchAdapter.registerCB(CheckPoint(modelTorchAdapter.config))
        modelTorchAdapter.registerCB(SystemCheckPoint(modelTorchAdapter.config))
        modelTorchAdapter.registerCB(FindLearningRate(modelTorchAdapter.config, 0.1, 5))

        modelTorchAdapter.epoch = 0
        modelTorchAdapter.runningStats = {}
        modelTorchAdapter.trigger("trainingStart", modelTorchAdapter, dataset)
        modelTorchAdapter.trigger("validationStart", modelTorchAdapter, dataset)
        modelTorchAdapter.trigger("validationEnd", modelTorchAdapter, scorerResults, scorer)

        modelTorchAdapter.model.state_dict.assert_called()
        modelTorchAdapter.optimizer.state_dict.assert_called()

        self.torchMock.save.assert_has_calls(
            [
                call(modelTorchAdapter.model.state_dict(), ".resumeModel.pt"),
                call(modelTorchAdapter.optimizer.state_dict(), ".resumeOptimizer.pt"),
            ]
        )
        open.assert_has_calls(
            [
                call(Path(".resumeAdapter.pcl"), "wb"),
                call().__enter__(),
                call().__exit__(None, None, None),
                call(Path(".resumeCheckpoint.pt"), "wb"),
                call().__enter__(),
                call().__exit__(None, None, None),
            ]
        )

    @patch("builtins.open.__enter__")
    @patch("builtins.open")
    @patch("pickle.load")
    @patch("pathlib.Path.exists", return_value=True)
    def test_restoreCheckpoint(self, pathExist, pickeLoad, open, openEnter):
        from pyPhasesML.adapter.ModelTorchAdapter import ModelTorchAdapter
        pickeLoad.side_effect = [
            {
                "epoch": 3,
                "bestMetric": 0.8,
            },
            {
                "notImprovedSince": 4,
                "metricDefinitions": {"a": 2},
                "bestModelPath": "here",
            },
        ]
        openEnter.side_effect = ["resumeAdapter.pt", "resumeCheckpoint.pt"]

        modelTorchAdapter = ModelTorchAdapter()
        modelTorchAdapter.model = MagicMock()
        modelTorchAdapter.bestModelPath = None
        modelTorchAdapter.config = MagicMock()
        modelTorchAdapter.config.logPath = "."
        modelTorchAdapter.config.stopAfterNotImproving = 0
        modelTorchAdapter.optimizer = MagicMock()
        dataset = MagicMock()

        cp = CheckPoint(modelTorchAdapter.config)

        modelTorchAdapter.registerCB(cp)
        modelTorchAdapter.registerCB(SystemCheckPoint(modelTorchAdapter.config))
        modelTorchAdapter.registerCB(FindLearningRate(modelTorchAdapter.config, 0.1, 5))

        modelTorchAdapter.trigger("trainingStart", modelTorchAdapter, dataset)

        self.torchMock.load.assert_has_calls(
            [
                call(".resumeModel.pt"),
                call(".resumeOptimizer.pt"),
            ]
        )

        self.assertEqual(cp.metricDefinitions, {"a": 2})
        self.assertEqual(cp.notImprovedSince, 4)
        self.assertEqual(modelTorchAdapter.metricDefinitions, {"a": 2})
        self.assertEqual(modelTorchAdapter.bestModelPath, "here")
        self.assertEqual(modelTorchAdapter.startEpoch, 3)
        self.assertEqual(modelTorchAdapter.bestMetric, 0.8)
