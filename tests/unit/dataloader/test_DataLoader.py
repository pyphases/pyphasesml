import unittest
from unittest.mock import MagicMock, patch

from pyPhasesML.datapipes import DataLoader, DataPipe
from pyPhasesML.datapipes.BatchMap import BatchMap
from pyPhasesML.datapipes.ShuffleMap import ShuffleMap
from pyPhasesML.datapipes.PreloadPipe import PreloadPipe

class TestDataLoader(unittest.TestCase):

    def test_build_no_options(self):
        data = MagicMock(spec=DataPipe)
        
        # Test no options
        loader = DataLoader.build(data, preload=False)
        self.assertEqual(loader, data)
        
    def test_build_batch(self):
        data = MagicMock(spec=DataPipe)
        
        loader = DataLoader.build(data, batchSize=32, preload=False)
        self.assertIsInstance(loader, BatchMap)
        self.assertEqual(loader.batchSize, 32)
        
    def test_build_shuffle(self):
        data = list(range(10))

        loader = DataLoader.build(data, shuffle=True, preload=False)
        self.assertIsInstance(loader, ShuffleMap)
        
    @patch("pyPhasesML.datapipes.PreloadPipe.PreloadPipe.start")
    def test_build_multithreading(self, _):
        data = [MagicMock(spec=DataPipe)]
        
        loader = DataLoader.build(data, preload=True)
        self.assertIsInstance(loader, PreloadPipe)
        
    # def test_build_torch(self):
        # data = MagicMock(spec=DataPipe)
        
        # loader = DataLoader.build(data, torch=True)
        # self.assertIsInstance(loader, TorchMap)
        
    @patch("pyPhasesML.datapipes.PreloadPipe.PreloadPipe.start")
    def test_build_All(self, _):
        data = list(range(10))
        
        loader = DataLoader.build(data, batchSize=16, shuffle=True, preload=True)
        self.assertIsInstance(loader, PreloadPipe)
        self.assertIsInstance(loader.datapipe, BatchMap)
        self.assertIsInstance(loader.datapipe.datapipe, ShuffleMap)
        self.assertEqual(loader.datapipe.datapipe.datapipe, data)
        
