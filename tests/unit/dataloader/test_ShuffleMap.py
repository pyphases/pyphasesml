import unittest
from pyPhasesML.datapipes.ShuffleMap import ShuffleMap

class TestShuffleMap(unittest.TestCase):

    def test_shuffle2(self):
        data = list(range(10))
        shufflePipe = ShuffleMap(data, seed=2)
                
        shuffledData = [d for d in shufflePipe]
        self.assertEqual(shuffledData, [5, 9, 3, 4, 6, 7, 2, 8, 1, 0])
        
    def test_shuffle3(self):
        data = list(range(10))
        shufflePipe = ShuffleMap(data, seed=3)
                
        shuffledData = [d for d in shufflePipe]
        self.assertEqual(shuffledData, [1, 5, 6, 0, 9, 4, 7, 2, 8, 3])
