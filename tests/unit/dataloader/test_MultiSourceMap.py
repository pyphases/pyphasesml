import unittest

from pyPhasesML.datapipes import MultiSourceMap
from pyPhasesML.datapipes.RecordMap import RecordMap


class TestMultiSourceMap(unittest.TestCase):
    def setUp(self):
        self.dataset = ["a", "b", "c", "d", "e", "f"]
        self.mapping1 = [0, 2, 4]
        self.mapping2 = [1, 3]
        self.mapping3 = [4, 5]

        self.map1 = RecordMap(self.dataset, self.mapping1)
        self.map2 = RecordMap(self.dataset, self.mapping2)
        self.map3 = RecordMap(self.dataset, self.mapping3)
        

        self.multiMapping = MultiSourceMap([self.map1, self.map2, self.map3])

    def test_len(self):
        self.assertEqual(len(self.multiMapping), 7)

    def test_getitem(self):
        self.assertEqual(self.multiMapping[0], "a")
        self.assertEqual(self.multiMapping[1], "c")
        self.assertEqual(self.multiMapping[2], "e")
        self.assertEqual(self.multiMapping[3], "b")
        self.assertEqual(self.multiMapping[4], "d")
        self.assertEqual(self.multiMapping[5], "e")
        self.assertEqual(self.multiMapping[6], "f")

    def test_iter(self):
        expected_output = ["a", "c", "e", "b", "d", "e", "f"]
        for i, item in enumerate(self.multiMapping):
            self.assertEqual(item, expected_output[i])
