import unittest

from pyPhasesML.datapipes.DatasetXY import DatasetXY

class TestDatasetXY(unittest.TestCase):

    def test_init(self):
        X = [1, 2, 3]
        Y = [4, 5, 6]
        dataset = DatasetXY(X, Y)
        
        self.assertEqual(len(dataset), 3)
        self.assertEqual(dataset[0], (1, 4))
        self.assertEqual(dataset[1], (2, 5))
        self.assertEqual(dataset[2], (3, 6))

    def test_len(self):
        X = [1, 2, 3]
        Y = [4, 5, 6]        
        dataset = DatasetXY(X, Y)
        
        self.assertEqual(len(dataset), len(X))

    def test_indexing(self):
        X = [1, 2, 3]
        Y = [4, 5, 6]        
        dataset = DatasetXY(X, Y)
        
        self.assertEqual(dataset[0], (1, 4))
        self.assertEqual(dataset[1], (2, 5))
        
