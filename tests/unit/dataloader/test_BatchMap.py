
import unittest
from unittest.mock import MagicMock, call

import numpy as np

from pyPhasesML.datapipes.BatchMap import BatchMap


class TestBatchMap(unittest.TestCase):

    def test_getitem(self):
        mock_pipe = MagicMock()
        mock_pipe.__getitem__.side_effect = lambda i: (i, i+1)
        mock_pipe.__len__.return_value = 10
        
        batch_map = BatchMap(mock_pipe, batchSize=3)
        
        x, y = batch_map[0]
        
        np.testing.assert_array_equal(x, np.array([0, 1, 2]))
        np.testing.assert_array_equal(y, np.array([1, 2, 3]))
        
        mock_pipe.__getitem__.assert_has_calls([
            call(0), call(1), call(2)
        ])
        
    def test_len(self):
        mock_pipe = MagicMock()
        mock_pipe.__len__.return_value = 10
        
        batch_map = BatchMap(mock_pipe, batchSize=3, onlyFullBatches=True)
        self.assertEqual(len(batch_map), 3)
        
        batch_map = BatchMap(mock_pipe, batchSize=3, onlyFullBatches=False)
        self.assertEqual(len(batch_map), 4)
        
        mock_pipe.__len__.return_value = 12

        batch_map = BatchMap(mock_pipe, batchSize=3, onlyFullBatches=True)
        self.assertEqual(len(batch_map), 4)

        batch_map = BatchMap(mock_pipe, batchSize=3, onlyFullBatches=False)
        self.assertEqual(len(batch_map), 4)
        