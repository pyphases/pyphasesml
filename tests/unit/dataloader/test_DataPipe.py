import unittest

from pyPhasesML.datapipes.DataPipe import DataPipe

class TestDataLoader(unittest.TestCase):

    def test_len(self):
        # Create a dataloader
        dataloader = DataPipe([1,2,3,4,5])
        
        # Check the length
        self.assertEqual(len(dataloader), 5)
        
    def test_getitem(self):
        # Create a dataloader
        dataloader = DataPipe([1,2,3,4,5])
        
        # Check indexing
        self.assertEqual(dataloader[0], 1)
        self.assertEqual(dataloader[2], 3)
        
    def test_iter(self):
        # Create a dataloader
        dataloader = DataPipe([1,2,3,4,5])
        
        # Check iteration
        expected = [1,2,3,4,5]
        for i, data in enumerate(dataloader):
            self.assertEqual(data, expected[i])
            
