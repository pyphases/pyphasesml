from time import sleep
import unittest

from pyPhasesML.datapipes.DataPipe import DataPipe
from pyPhasesML.datapipes.PreloadPipe import PreloadPipe


class DelayedDataPipe:
    def __init__(self, data, delays=[]):
        self.data = data
        self.delays = delays

    def __getitem__(self, index):
        sleep_time = self.delays[index]  # Random sleep time between 0.01 and 0.2 seconds
        if index == 1:
            sleep_time = 0.01  # Make sure the second element loads faster
        sleep(sleep_time)
        return self.data[index]

    def __len__(self):
        return len(self.data)


class TestPreloadPipe(unittest.TestCase):
        
    def test_all_elements_processed_in_order(self):
        datapipe = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        preload = PreloadPipe(datapipe)
        self.assertEqual(list(preload), datapipe)

    def test_preload_count_less_than_datapipe_twicheh(self):
        datapipe = [0, 1, 2, 3, 4, 5]
        preload = PreloadPipe(datapipe)
        self.assertEqual(list(preload), datapipe)
        self.assertEqual(list(preload), datapipe)

    def test_empty_datapipe(self):
        datapipe = []
        datapipe = []
        preload = PreloadPipe(datapipe)
        self.assertEqual(list(preload), datapipe)

    def test_empty_datapipe_delayed(self):
        listValues = [0, 1, 2]
        datapipe = DelayedDataPipe(listValues, [0.5, 0, 0])
        preload = PreloadPipe(datapipe)
        self.assertEqual(list(preload), listValues)

    def test_all_elements_processed_preloadCount(self):
        datapipe = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        preload = PreloadPipe(datapipe, preloadCount=3)
        self.assertEqual(list(preload), datapipe)
        
    def test_empty_datapipe_delayed_preloadCount(self):
        listValues = [0, 1, 2]
        datapipe = DelayedDataPipe(listValues, [1, 0.5, 0.5])
        preload = PreloadPipe(datapipe, preloadCount=3)
        self.assertEqual(list(preload), listValues)
