import unittest
from unittest.mock import MagicMock, patch

from pyPhasesML.adapter.torch.CheckPoint import CheckPoint
from pyPhasesML.Model import ModelConfig


@patch.dict('sys.modules', torch=MagicMock())
class TestCheckPoint(unittest.TestCase):

    def setUp(self):
        self.checkpoint = CheckPoint(ModelConfig())

    def test_onTrainingStart(self):
        model = MagicMock()
        dataset = MagicMock()
        self.checkpoint.onTrainingStart(model, dataset)
        self.assertIsNotNone(self.checkpoint.epochStartTime)

    def test_onValidationStart(self):
        model = MagicMock()
        dataset = MagicMock()
        self.checkpoint.onValidationStart(model, dataset)
        self.assertIsNotNone(self.checkpoint.epochEndTime)
    
    @patch('builtins.open')
    @patch('torch.cuda.max_memory_allocated', return_value=12)
    def test_on_validation_end(self, openMock, mock2):
        model = MagicMock()
        model.epoch = 1
        model.bestMetric = 0.7
        model.validationMetrics = ['accuracy']
        
        results = {'accuracy': 0.8}
        
        scorer = MagicMock()
        scorer.metrics = ['accuracy']
        scorer.getMetricDefinition.return_value = [0.7, True, True]
        
        checkpoint = self.checkpoint
        checkpoint.trigger("trainingStart", None, None)
        checkpoint.trigger("validationStart", None, None)
        checkpoint.onValidationEnd(model, results, scorer)
        
        self.assertEqual(model.bestMetric, 0.8)
        self.assertEqual(checkpoint.metricDefinitions, {'accuracy': [0.8, True, True]})
        self.assertTrue(model.bestModelPath.endswith('0.800.pkl'))
        
        results = {'accuracy': 0.7}
        checkpoint.onValidationEnd(model, results, scorer)
        
        self.assertEqual(model.bestMetric, 0.8)
        self.assertEqual(checkpoint.metricDefinitions, {'accuracy': [0.8, True, True]})
        self.assertTrue(model.bestModelPath.endswith('0.800.pkl'))
        
        results = {'accuracy': 0.85}
        checkpoint.onValidationEnd(model, results, scorer)
        
        self.assertEqual(model.bestMetric, 0.85)
        self.assertEqual(checkpoint.metricDefinitions, {'accuracy': [0.85, True, True]})
        self.assertTrue(model.bestModelPath.endswith('0.850.pkl'))
    
