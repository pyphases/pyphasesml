from pathlib import Path
from unittest import TestCase

import numpy as np
import numpy.testing as npt

from pyPhasesML.exporter.MemmapRecordExporter import MemmapRecordExporter
from pyPhasesML.exporter.MemmapRecordSegmentsExporter import MemmapRecordSegmentsExporter


class TestMemmapRecordExporter(TestCase):
    numpyExporter = MemmapRecordExporter({"basePath": "tests/data/"})

    @classmethod
    def setUpClass(cls) -> None:
        # setup testdata dir
        p = Path("tests/data")
        p.mkdir(parents=True, exist_ok=True)
        [file.unlink() for file in p.glob("*") if file.is_file()]

    def setUp(self) -> None:
        super().setUp()


    def test_loadingSegments(self):
        exporter = self.numpyExporter

        exporter.saveFromArrayList(
            "test",
            [
                np.array([1, 2, 3, 2]).reshape(-1, 1),
                np.array([4, 5, 4, 3, 2, 1]).reshape(-1, 1),
                np.array([6, 7]).reshape(-1, 1),
            ],
        )

        exporter.read("test", {"dtype": "float32"})

        exporter = MemmapRecordSegmentsExporter(exporter, segmentLength=2)

        npt.assert_equal(exporter[0][:, 0], [1, 2])
        npt.assert_equal(exporter[1][:, 0], [3, 2])
        npt.assert_equal(exporter[2][:, 0], [4, 5])
        npt.assert_equal(exporter[3][:, 0], [4, 3])
        npt.assert_equal(exporter[4][:, 0], [2, 1])
        npt.assert_equal(exporter[5][:, 0], [6,7])

    def test_loadingSegmentsShortOverflow(self):
        exporter = self.numpyExporter

        exporter.saveFromArrayList(
            "test",
            [
                np.array([1, 2, 3]).reshape(-1, 1),
                np.array([4, 5]).reshape(-1, 1),
                np.array([6, 7, 8]).reshape(-1, 1),
            ],
        )

        exporter.read("test", {"dtype": "float32"})
        
        exporter = MemmapRecordSegmentsExporter(exporter, segmentLength=2)

        npt.assert_equal(exporter[0][:, 0], [1, 2])
        npt.assert_equal(exporter[1][:, 0], [3, 4])
        npt.assert_equal(exporter[2][:, 0], [4, 5])
        npt.assert_equal(exporter[3][:, 0], [6, 7])
        npt.assert_equal(exporter[4][:, 0], [])
