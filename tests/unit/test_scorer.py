import unittest

import numpy as np
import numpy.testing as npt
from sklearn.metrics import cohen_kappa_score, roc_auc_score, average_precision_score, confusion_matrix as skConfusionMatrix
from pyPhasesML.scorer.Scorer import Scorer, confusion_matrix

from sklearn.metrics import f1_score, mean_squared_error


class TestScorer(unittest.TestCase):
    classes = np.array([0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0])
    prediction = np.array([0.1, 0.8, 0.8, 0.3, 0.6, 0.7, 0.8, 1, 0.3, 0.2, 0.1, 0.0])

    def getPredictedClasses(self):
        classes = self.prediction.copy()
        classes[classes >= 0.5] = 1
        classes[classes < 0.5] = 0
        return classes

    def hotEncodedPrediction(self):
        classes = np.empty((len(self.prediction), 2))
        classes[:, 0] = 1 - self.prediction
        classes[:, 1] = self.prediction
        return classes
    
    def test_flattens_and_squeezes_array(self):
        # arrange
        input_array = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
        num_classes = 3
        scorer = Scorer(num_classes)
        
        # act
        result = scorer.prepareTruth(input_array)
        
        # assert
        expected_result = np.array([0, 1, 2])
        assert np.array_equal(result, expected_result)
    
    def test_handles_ignored_values(self):
        # arrange
        input_array = np.array([[0, 0, 0], [1, 0, 0], [0, 0, 1]])
        num_classes = 3
        scorer = Scorer(num_classes)
        
        # act
        result = scorer.prepareTruth(input_array)
        
        # assert
        expected_result = np.array([-1, 0, 2])
        assert np.array_equal(result, expected_result)
        
    def test_scoreAllRecordsRaisesErrorIfNoTrace(self):
        # arrange
        scorer = Scorer(2)
        
        # act
        with self.assertRaises(Exception) as context:
            scorer.scoreAllRecords()
            
        # assert
        self.assertTrue("scorer.trace to True" in str(context.exception))
        
    def test_maskedIgnoredValues(self):
        # arrange
        truth = np.array([-1, 0, 1, 2])
        prediction = np.array([[0.1, 0.4, 0.3], [0.1, 0.2, 0.3], [0.4, 0.5, 0.6], [0.7, 0.8, 0.9]])
        num_classes = 3
        scorer = Scorer(num_classes)
        scorer.ignoreClasses = [-1]
        
        # act
        result_truth, result_pred = scorer.maskedIgnoredValues(truth, prediction)
        
        # assert
        expected_truth = np.array([0, 1, 2])
        expected_prediction = np.array([[0.1, 0.2, 0.3], [0.4, 0.5, 0.6], [0.7, 0.8, 0.9]])
        npt.assert_equal(result_truth, expected_truth)
        npt.assert_equal(result_pred, expected_prediction)

    def test_returns_correct_predictions(self):
        # arrange
        prediction = np.array([[0.1, 0.8, 0.1], [0.3, 0.3, 0.4], [0.6, 0.2, 0.2]])
        num_classes = 3
        threshold = 0.5
        instance = Scorer(num_classes, threshold=threshold)
        
        # act
        result = instance.getClassPrediction(prediction)
        
        # assert
        expected_result = np.array([1, 0, 0])
        assert np.array_equal(result, expected_result)
        
    def test_handles_threshold_correctly(self):
        # arrange
        prediction = np.array([[0.1, 0.8, 0.1], [0.3, 0.3, 0.4], [0.6, 0.2, 0.2]])
        num_classes = 3
        threshold = 0.7
        instance = Scorer(num_classes, threshold=threshold)
        
        # act
        result = instance.getClassPrediction(prediction)
        
        # assert
        expected_result = np.array([0, 0, 0])
        assert np.array_equal(result, expected_result)
        
    
    def test_returns_flattened_array_for_one_hot_encoded_input(self):
        # arrange
        prediction = np.array([[0, 1, 0], [1, 0, 0], [0, 0, 1]])
        num_classes = 3
        instance = Scorer(num_classes)
        
        # act
        result = instance.flattenPrediction(prediction)
        
        # assert
        expected_result = np.array([1, 0, 2])
        assert np.array_equal(result, expected_result)

    def test_returns_flattened_array_for_non_one_hot_encoded_input(self):
        # arrange
        prediction = np.array([0.1, 0.8, 0.3, 0.7, 0.6, 0.4])
        num_classes = 2
        instance = Scorer(num_classes)
        
        # act
        result = instance.flattenPrediction(prediction)
        
        # assert
        expected_result = np.array([0, 1, 0, 1, 1, 0])
        assert np.array_equal(result, expected_result)

    def test_returns_class_prediction(self):
        # arrange
        prediction = np.array([[0.9, 0.1], [0.2, 0.8], [0.3, 0.7]])
        num_classes = 2
        instance = Scorer(num_classes, threshold=0.5)
        
        # act
        result = instance.flattenPrediction(prediction, threshold=0.5)
        
        # assert
        expected_result = np.array([0, 1, 1])
        npt.assert_array_equal(result, expected_result)
    
    def test_raises_exception_for_binary_metric_with_multiclass_input(self):
        # arrange
        prediction = np.array([0.1, 0.5, 0.4, 0.3, 0.2, 0.5])
        num_classes = 3
        instance = Scorer(num_classes)
        binaryLikelyHood = True
        
        # assert
        with self.assertRaises(Exception) as context:
            instance.flattenPrediction(prediction, binaryLikelyHood=binaryLikelyHood)
        self.assertTrue("binary" in str(context.exception))
        
    def test_getMetricScorer(self):
        # arrange
        prediction = np.array([0.1, 0.5, 0.4, 0.3, 0.2, 0.5])
        truth = np.array([0, 1, 0, 1, 1, 0])
        num_classes = 2
        scorer = Scorer(num_classes)
        
        scorerF = scorer.getMetricScorer("accuracy")
        r = scorerF(truth, prediction)
        self.assertEqual(r, 0.5)
            
    def testAUPRC(self):
        true = average_precision_score(self.classes, self.prediction)

        s = Scorer(2, trace=True)
        s.metrics = ["auprc"]

        r = s.score(self.classes, self.hotEncodedPrediction())
        assert r["auprc"] == true

    def testAUPRCCombine(self):
        classes = np.concatenate([self.classes, self.classes])
        predictions = np.concatenate([self.prediction, self.prediction])
        true = average_precision_score(classes, predictions)

        s = Scorer(2, trace=True)
        s.metrics = ["auprc"]

        s.score(self.classes, self.hotEncodedPrediction(), trace=True)
        s.score(self.classes, self.hotEncodedPrediction(), trace=True)
        assert s.scoreAllRecords()["auprc"] == true

    def testAUROC(self):
        true = roc_auc_score(self.classes, self.prediction)
        s = Scorer(2)
        s.metrics = ["auroc"]

        r = s.score(self.classes, self.hotEncodedPrediction())
        assert r["auroc"] == true

    def testKappa(self):
        y_true = [self.classes, [0, 0, 1, 1, 0, 0, 1, 1]]
        y_pred = [self.getPredictedClasses(), [0, 1, 0, 1, 1, 1, 1, 0]]
        expected_scores = cohen_kappa_score(y_true[0], y_pred[0]), cohen_kappa_score(y_true[1], y_pred[1])
        expected_score = cohen_kappa_score(np.concatenate(y_true), np.concatenate(y_pred))
        s = Scorer(2)
        s.metrics = ["kappa"]
        self.checkScore("kappa", y_true, y_pred, expected_scores, expected_score)
        
    def testKappaNoTNFN(self):
        y_true = np.array([0, 0, 1, 1])
        y_pred = np.array([1, 1, 1, 1])
        s = Scorer(2)
        s.results = {}
        r = s.scoreMetric("kappa", y_true, y_pred)
        self.assertEqual(r, 0.0)
        
    def testKappaNoTNFP(self):
        y_true = np.array([1, 1, 1, 1])
        y_pred = np.array([0, 0, 1, 1])
        s = Scorer(2)
        s.results = {}
        r = s.scoreMetric("kappa", y_true, y_pred)
        self.assertEqual(r, 0.0)
        
    def testKappaNoTP(self):
        y_true = np.array([1, 1, 0, 0])
        y_pred = np.array([0, 0, 0, 0])
        s = Scorer(2)
        s.results = {}
        r = s.scoreMetric("kappa", y_true, y_pred)
        self.assertEqual(r, 0.0)
        
    def testKappaNoEmpty(self):
        y_true = np.array([])
        y_pred = np.array([])
        s = Scorer(2)
        s.results = {}
        r = s.scoreMetric("kappa", y_true, y_pred)
        self.assertEqual(r, 0.0)
        
    def testKappa1(self):
        y_true = np.array([1, 1, 0, 0])
        y_pred = np.array([1, 1, 0, 0])
        s = Scorer(2)
        s.results = {}
        r = s.scoreMetric("kappa", y_true, y_pred)
        self.assertEqual(r, 1.0)

    def testConfusion(self):
        truth = np.array([0, 1, 3, 2, 2, 2, 0, 2, 2, 2, 2, 3, 0])
        prediction = np.array([1, 1, 3, 1, 0, 2, 2, 2, 2, 2, 2, 2, 0])

        conf = skConfusionMatrix(truth, prediction)
        s = Scorer(4)
        s.metrics = ["confusion"]

        r = s.score(truth, prediction)
        npt.assert_array_equal(r["confusion"], conf)

    def testConfusionMissingPrediction(self):
        truth = np.array([0, 1, 3, 2, 2, 2, 0, 2, 2, 2, 2, 3, 0])
        prediction = np.array([1, 1, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0])

        conf = skConfusionMatrix(truth, prediction)
        s = Scorer(4)
        s.metrics = ["confusion"]

        r = s.score(truth, prediction)
        npt.assert_array_equal(r["confusion"], conf)

    def testConfusionMissingPrediction2(self):
        truth = np.array([0, 0])
        prediction = np.array([1, 1])

        conf = skConfusionMatrix(truth, prediction, labels=[0, 1, 2, 3])
        s = Scorer(4)
        s.metrics = ["confusion"]

        r = s.score(truth, prediction)
        npt.assert_array_equal(r["confusion"], conf)

    def testConfusionNoLabel(self):
        truth = np.array([0, 1, 2, 3])
        prediction = np.array([1, 1, 1, 1])

        confExpected = skConfusionMatrix(truth, prediction)
        confusion = confusion_matrix(truth, prediction)

        npt.assert_array_equal(confusion, confExpected)

    def test_getMetric(self):
        s = Scorer(4)
        value, useAsBest, biggerIsBetter = s.getMetricDefinition("kappa")

        self.assertEqual(value, -1)
        self.assertEqual(useAsBest, True)
        self.assertEqual(biggerIsBetter, True)

    def test_customMetric(self):
        Scorer.registerMetric("myMetric", lambda t, p: sum(t + p), lambda t, p: 2*(sum(t) + sum(p)), initValue=0)

        s = Scorer(3)
        s.trace = True
        s.metrics = ["myMetric"]
        value, useAsBest, biggerIsBetter = s.getMetricDefinition("myMetric")
        s.score(np.array([0]), np.array([1]))
        s.score(np.array([2]), np.array([1]))
        s.score(np.array([1]), np.array([1]))
        result = s.scoreAllRecords()

        self.assertEqual(value, 0)
        self.assertEqual(useAsBest, True)
        self.assertEqual(biggerIsBetter, True)
        
        self.assertIn("myMetric", result)
        self.assertEqual(s.recordResult[0]["myMetric"], 1)
        self.assertEqual(s.recordResult[1]["myMetric"], 3)
        self.assertEqual(s.recordResult[2]["myMetric"], 2)
        self.assertEqual(result["myMetric"], 12)
        
    def test_falseAUC(self):
        s = Scorer(2)
        noTP = [0, 0, 0, 0], [1, 1, 1, 1]
        noTP = s._auc([0, 0, 0, 0],  [1, 1, 1, 1])
        noTnoFPP = s._auc([1, 1, 1, 1],  [0, 0, 0, 0])
        
        npt.assert_equal(noTP[0], np.nan)
        npt.assert_equal(noTnoFPP[0], np.nan)
        
    def checkScore(self, score, truth_records, predicted_records, expected, expectedcombined, numClasses=2):
        y_true_r0, y_true_r1 = truth_records
        y_pred_r0, y_pred_r1 = predicted_records
        scorer = Scorer(numClasses)
        scorer.trace = True
        scorer.metrics = [score]    
        
        scorer.score(np.array(y_true_r0), np.array(y_pred_r0))
        scorer.score(np.array(y_true_r1), np.array(y_pred_r1))
        
        results = scorer.scoreAllRecords()
        
        self.assertIn(score, results)
        npt.assert_almost_equal(scorer.recordResult[0][score], expected[0])
        npt.assert_almost_equal(scorer.recordResult[1][score], expected[1])
        npt.assert_almost_equal(results[score], expectedcombined)
        
        return scorer
        
    def test_f1_score(self):
        y_true = [1, 0, 1, 1, 0, 1], [1, 0, 1, 1, 0, 1]
        y_pred = [0, 1, 1, 1, 1, 0], [0, 1, 0, 1, 0, 0]
        expected_score = f1_score(y_true[0], y_pred[0]), f1_score(y_true[1], y_pred[1])
        
        self.checkScore("f1", y_true, y_pred, expected_score, f1_score(np.concatenate(y_true), np.concatenate(y_pred)))
    
    def test_f1_score_multisclass(self):
        y_true = [1, 2, 2, 2, 1, 1, 0, 0, 0, 1, 1, 0, 1], [1, 0, 1, 1, 2, 1]
        y_pred = [0, 1, 1, 2, 1, 0, 0, 0, 0, 1, 2, 0, 1], [0, 1, 0, 1, 0, 0]
        
        expected_score = f1_score(y_true[0], y_pred[0], average="macro"), f1_score(y_true[1], y_pred[1], average="macro")
        expected_combine = f1_score(np.concatenate(y_true), np.concatenate(y_pred), average="macro")
        
        scorer = self.checkScore("f1", y_true, y_pred, expected_score, expected_combine, numClasses=3)
        npt.assert_almost_equal(scorer.results["f1-0"], 0.533333333333)
        npt.assert_almost_equal(scorer.results["f1-1"], 0.47058823529411764)
        npt.assert_almost_equal(scorer.results["f1-2"], 0.333333333333)
        
    def test_meanSquared_score(self):
        y_true = [1, 0, 1, 1, 0, 1], [1, 0, 1, 1, 0, 1]
        y_pred = [0, 0.5, 0.7, 0, 0.7, 0], [0, 1, 0, 1, 0, 0]
        
        expected_score = mean_squared_error(y_true[0], y_pred[0]), mean_squared_error(y_true[1], y_pred[1])
        expected_combine = mean_squared_error(np.concatenate(y_true), np.concatenate(y_pred))
        
        self.checkScore("meanSquared", y_true, y_pred, expected_score, expected_combine)
        
    def test_meanSquared_scoreMulti(self):
        y_true = [1, 2], [1, 0]
        y_pred = [[0.3, 0.7, 0.8], [0.2, 0.5, 0.3]], [[0.3, 0.7, 0.8], [0.2, 0.5, 0.3]]
        y_trueHot = [[0, 1, 0], [0, 0, 1]], [[0, 1, 0], [1, 0, 0]]
        
        expected_score = mean_squared_error(y_trueHot[0], y_pred[0]), mean_squared_error(y_trueHot[1], y_pred[1])
        expected_combine = mean_squared_error(np.concatenate(y_trueHot), np.concatenate(y_pred))
        
        self.checkScore("meanSquared", y_true, y_pred, expected_score, expected_combine, numClasses=3)
        
    