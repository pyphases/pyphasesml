import unittest
from unittest.mock import MagicMock

import numpy as np

from pyPhasesML import DataSet, TrainingSetLoader


class TestDataSet(unittest.TestCase):

    def setUp(self):
        self.x = np.array([[1, 2], [3, 4]])
        self.y = np.array([0, 1])
        self.numClasses = 2
        self.data = DataSet(self.x, self.y, self.numClasses)

    def test_init(self):
        self.assertEqual(self.data.x.tolist(), self.x.tolist())
        self.assertEqual(self.data.y.tolist(), self.y.tolist())
        self.assertEqual(self.data.numClasses, self.numClasses)
        self.assertIsNone(self.data.catMatrix)

    def test_len(self):
        self.assertEqual(len(self.data), 2)

    def test_fromTuple(self):
        data_tuple = (self.x, self.y)
        result = DataSet.fromTuple(data_tuple, self.numClasses)
        self.assertIsInstance(result, DataSet)
        self.assertEqual(result.x.tolist(), self.x.tolist())
        self.assertEqual(result.y.tolist(), self.y.tolist())
        self.assertEqual(result.numClasses, self.numClasses)

    def test_asTuple(self):
        result = self.data.asTuple()
        self.assertIsInstance(result, tuple)
        self.assertEqual(result[0].tolist(), self.x.tolist())
        self.assertEqual(result[1].tolist(), self.y.tolist())
        self.assertEqual(result[2], self.numClasses)

    def test_getitem(self):
        result_x = self.data[0]
        self.assertEqual(result_x.tolist(), self.x.tolist())

        result_y = self.data[1]
        self.assertEqual(result_y.tolist(), self.y.tolist())

        with self.assertRaises(StopIteration):
            self.data[2]

        with self.assertRaises(TypeError):
            self.data[3]


class TestTrainingSetLoader(unittest.TestCase):

    def setUp(self):
        self.training_data = MagicMock()
        self.validation_data = MagicMock()
        self.loader = TrainingSetLoader(self.training_data, self.validation_data)

    def test_init(self):
        self.assertEqual(self.loader.trainingData, self.training_data)
        self.assertEqual(self.loader.validationData, self.validation_data)
