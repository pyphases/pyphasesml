import unittest
from unittest.mock import MagicMock

from pyPhasesML.Model import AdapterNotImplemented, MetricsDoesNotExist, Model


class MockModel(Model):
    def __init__(self):
        super().__init__()
        self.model = "mock model"

    def train(self):
        pass

    def predict(self, input, get_likelihood=False, returnNumpy=True):
        pass


class TestModel(unittest.TestCase):
    def setUp(self):
        self.model = MockModel()
        # don't judge, but i need the 100%
        self.model.train()
        self.model.predict(None)

    def test_get_csv_path(self):
        self.assertEqual(self.model.getCsvPath(), "logs/log.csv")

    def test_get_metric(self):
        self.assertEqual(self.model.getMetric("acc"), {"name": "acc", "type": "max"})

    def test_get_metric_not_defined(self):
        with self.assertRaises(MetricsDoesNotExist):
            self.model.getMetric("foo")

    def test_train_not_implemented(self):
        with self.assertRaises(Exception):
            Model().train()

    def test_predict_not_implemented(self):
        with self.assertRaises(Exception):
            Model().predict(None)

    def test_getMetric(self):
        self.assertEqual(self.model.getMetric("acc"), {"name": "acc", "type": "max"})
        self.assertRaises(MetricsDoesNotExist, self.model.getMetric, "foo")

    def test_prepareData(self):
        dataset = ([1, 2, 3], [4, 5, 6])
        x, y = self.model.prepareData(dataset)
        self.assertEqual(x, [1, 2, 3])
        self.assertEqual(y, [4, 5, 6])

    def test_prepareX(self):
        x = [1, 2, 3]
        self.assertEqual(self.model.prepareX(x), x)

    def test_prepareY(self):
        y = [4, 5, 6]
        self.assertEqual(self.model.prepareY(y), y)

    def test_getLossFunction(self):
        self.assertRaises(AdapterNotImplemented, self.model.getLossFunction)

    def test_getLossWeights(self):
        self.assertIsNone(self.model.getLossWeights())

    def test_getModelEval(self):
        self.model.modelEval = MagicMock()
        self.assertEqual(self.model.getModelEval(), self.model.modelEval)
        self.model.modelEval.eval.assert_called_once_with()

    def test_getModelDebug(self):
        self.model.modelDebug = "foo"
        self.assertEqual(self.model.getModelDebug(), "foo")

    def test_mapOutput(self):
        outputData = [1, 2, 3]
        self.assertEqual(self.model.mapOutput(outputData), outputData)

    def test_mapPrediction(self):
        output = [1, 2, 3]
        self.assertEqual(self.model.mapPrediction(output), output)

    def test_mapOutputForPrediction(self):
        output = [[[1, 0], [0.7, 0.3]], [[0.4, 0.6], [0.9, 0.1]]]
        self.assertEqual(self.model.mapOutputForPrediction(output), output)
