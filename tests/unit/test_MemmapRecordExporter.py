from pathlib import Path
from unittest import TestCase

import numpy as np
import numpy.testing as npt

from pyPhasesML.exporter.MemmapRecordExporter import MemmapRecordExporter


class TestMemmapRecordExporter(TestCase):
    numpyExporter = MemmapRecordExporter({"basePath": "tests/data/"})

    @classmethod
    def setUpClass(cls) -> None:
        # setup testdata dir
        p = Path("tests/data")
        p.mkdir(parents=True, exist_ok=True)
        [file.unlink() for file in p.glob("*") if file.is_file()]

    def setUp(self) -> None:
        super().setUp()

    def test_saveFromArrayList(self):
        exporter = self.numpyExporter

        exporter.saveFromArrayList(
            "test",
            [
                np.array([1, 2, 3]).reshape(-1, 1),
                np.array([4, 5]).reshape(-1, 1),
                np.array([6, 7, 8]).reshape(-1, 1),
            ],
        )

        lengths, shape = np.load(exporter.getShapeFilePath("test"), allow_pickle=True)
        array = np.memmap(exporter.getPath("test"), dtype="float32", mode="r", shape=(1, 8, 1))

        self.assertEqual(array.shape, (1, 8, 1))
        npt.assert_equal(shape, [1, 8, 1])
        npt.assert_equal(lengths, [3, 2, 3])
        npt.assert_equal(array.flatten(), np.array([1, 2, 3, 4, 5, 6, 7, 8]).flatten())

    def test_saveAndAppendArray(self):
        exporter = self.numpyExporter

        exporter.saveAndAppendArray(
            "test",
            [np.array([2, 3, 4]).reshape(-1, 1), np.array([5, 6]).reshape(-1, 1)],
        )
        exporter.saveAndAppendArray("test", [np.array([7, 8, 9]).reshape(-1, 1)])
        exporter.finishStream("test")

        lengths, shape = np.load(exporter.getShapeFilePath("test"), allow_pickle=True)
        array = np.memmap(exporter.getPath("test"), dtype="float32", mode="r", shape=(1, 8, 1))

        self.assertEqual(array.shape, (1, 8, 1))
        npt.assert_equal(shape, [1, 8, 1])
        npt.assert_equal(lengths, [3, 2, 3])
        npt.assert_equal(array.flatten(), np.array([2, 3, 4, 5, 6, 7, 8, 9]).flatten())

    def test_saveAndAppendArrayMultiplyIds(self):
        exporter = self.numpyExporter

        exporter.saveAndAppendArray(
            "test1",
            [np.array([1, 3, 2, 1, 2, 3]).reshape(-1, 2)],
        )
        exporter.saveAndAppendArray(
            "test2",
            [np.array([2, 3, 4]).reshape(-1, 1), np.array([5, 6]).reshape(-1, 1)],
        )
        exporter.saveAndAppendArray("test2", [np.array([7, 8, 9]).reshape(-1, 1)])
        exporter.finishStream("test2")
        exporter.finishStream("test1")

        lengths, shape = np.load(exporter.getShapeFilePath("test2"), allow_pickle=True)
        array = np.memmap(exporter.getPath("test2"), dtype="float32", mode="r", shape=(1, 8, 1))

        self.assertEqual(array.shape, (1, 8, 1))
        npt.assert_equal(shape, [1, 8, 1])
        npt.assert_equal(lengths, [3, 2, 3])
        npt.assert_equal(array.flatten(), np.array([2, 3, 4, 5, 6, 7, 8, 9]).flatten())

        lengths, shape = np.load(exporter.getShapeFilePath("test1"), allow_pickle=True)
        array = np.memmap(exporter.getPath("test1"), dtype="float32", mode="r", shape=(1, 3, 2))

        self.assertEqual(array.shape, (1, 3, 2))
        npt.assert_equal(shape, [1, 3, 2])
        npt.assert_equal(lengths, [3])
        npt.assert_equal(array.flatten(), np.array([1, 3, 2, 1, 2, 3]).flatten())

