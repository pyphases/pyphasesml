import unittest

import numpy as np
import numpy.testing as npt

from pyPhasesML.DataManipulation import DataManipulation


class TestDataManipulation(unittest.TestCase):
    def setUp(self):
        # create an instance of the DataManipulation class for testing
        self.config = {"manipulation": []}
        self.splitName = "testing"

        class MyDataManipulation(DataManipulation):
            def return_0_1(self, X, Y):
                return 0, 1
            def return_x_y(self, X, Y):
                return X, Y
            def plus1(self, X, Y):
                return X+1, Y+1

        self.dataManipulation = MyDataManipulation(self.config["manipulation"], self.splitName, self.config)

    def test_manipulateSegment(self):
        # test manipulateSegment method with some input data
        X = np.zeros((100, 2))
        Y = np.zeros((100, 2))
        X_manipulated, Y_manipulated = self.dataManipulation((X, Y))
        # make sure the output shape is the same as the input shape
        self.assertEqual(X_manipulated.shape, (100, 2))
        self.assertEqual(Y_manipulated.shape, (100, 2))

    def test_manipulateSegmentBatchDimension(self):
        # test manipulateSegment method with some input data
        X = np.zeros((100, 2))
        Y = np.zeros((100, 2))
        
        config = [{"name": "addBatchDimension"}]
        X_manipulated, Y_manipulated = self.dataManipulation.manipulateByConfig(X, Y, config)
        # make sure the output shape is the same as the input shape
        self.assertEqual(X_manipulated.shape, (1, 100, 2))
        self.assertEqual(Y_manipulated.shape, (1, 100, 2))

    def test_manipulateByConfig(self):
        # test manipulateByConfig method with some input data and a configuration
        X = np.zeros((10, 100, 2))
        Y = np.zeros((10, 1))
        config = [{"name": "return_0_1"}]
        X_manipulated, Y_manipulated = self.dataManipulation.manipulateByConfig(X, Y, config)
        # make sure the output shape is the same as the input shape
        self.assertEqual(X_manipulated, 0)
        self.assertEqual(Y_manipulated, 1)

    def test_manipulateBatchesByConfig(self):
        # test manipulateBatchesByConfig method with some input data and a configuration
        X = np.zeros((10, 100, 2))
        Y = np.zeros((10, 1))
        config = [{"name": "return_x_y"}]
        X_manipulated, Y_manipulated = self.dataManipulation.manipulateByConfig(X, Y, config)
        # make sure the output shape is the same as the input shape
        self.assertEqual(X_manipulated.shape, X.shape)
        self.assertEqual(Y_manipulated.shape, Y.shape)

    def test_loadFromConfig(self):
        # test loadFromConfig method with some input data and a configuration
        X = np.zeros((10, 100, 2))
        Y = np.zeros((10, 1))
        config = {"name": "return_0_1"}
        X_manipulated, Y_manipulated = self.dataManipulation.loadFromConfig(config, X, Y, self.splitName)
        # make sure the output shape is the same as the input shape
        self.assertEqual(X_manipulated, 0)
        self.assertEqual(Y_manipulated, 1)

    def test_call(self):
        # test manipulateByConfig method with some input data and a configuration
        X = np.zeros((10, 100, 2))
        Y = np.zeros((10, 1))
        config = [{"name": "return_0_1"}]
        X_manipulated, Y_manipulated = self.dataManipulation((X, Y), config)
        # make sure the output shape is the same as the input shape
        self.assertEqual(X_manipulated, 0)
        self.assertEqual(Y_manipulated, 1)

    def test_trainingOnlyTesting(self):
        # test manipulateByConfig method with some input data and a configuration
        X = np.zeros((10, 100, 2))
        Y = np.zeros((10, 1))
        config = {"name": "return_0_1", "trainingOnly": True}
        X_manipulated, Y_manipulated = self.dataManipulation.loadFromConfig(config, X, Y, "test")
        # make sure the output shape is the same as the input shape
        npt.assert_equal(X_manipulated, X)
        npt.assert_equal(Y_manipulated, Y)

    def test_trainingOnlyTraining(self):
        # test manipulateByConfig method with some input data and a configuration
        X = np.zeros((10, 100, 2))
        Y = np.zeros((10, 1))
        config = {"name": "return_0_1", "trainingOnly": True}
        X_manipulated, Y_manipulated = self.dataManipulation.loadFromConfig(config, X, Y, "training")
        # make sure the output shape is the same as the input shape
        npt.assert_equal(X_manipulated, 0)
        npt.assert_equal(Y_manipulated, 1)
    
    def test_ignoreChannels(self):
        # test manipulateByConfig method with some input data and a configuration
        X = np.zeros((10, 100, 2))
        Y = np.zeros((10, 1))
        config = {"name": "plus1", "ignoreChannels": [1]}
        X_manipulated, Y_manipulated = self.dataManipulation.loadFromConfig(config, X, Y, "training")
        # make sure the output shape is the same as the input shape
        npt.assert_equal(X_manipulated[:, :, 1], X[:, :, 1])
        npt.assert_equal(Y_manipulated[:, 0], 1)
        npt.assert_equal(X_manipulated[:, :, 0], 1)

    def test_noStepException(self):
        # test manipulateByConfig method with some input data and a configuration
        X = np.zeros((10, 100, 2))
        Y = np.zeros((10, 1))
        config = {"name": "nonExisting"}
        self.assertRaises(Exception, self.dataManipulation.loadFromConfig, config, X, Y, "training")
